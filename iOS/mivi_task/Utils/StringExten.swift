//
//  StringExten.swift
//  mivi_task
//
//  Created by Gnanavadivelu Pandurangan on 20/08/18.
//  Copyright © 2018 Gnanavadivelu Pandurangan. All rights reserved.
//

import Foundation

public extension String {
    
    public var isEmail: Bool {
        // http://stackoverflow.com/questions/25471114/how-to-validate-an-e-mail-address-in-swift
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    public var whiteSpace: Bool?{
        return self == " "
    }
    public var length: Int {
        return characters.count
    }
}

