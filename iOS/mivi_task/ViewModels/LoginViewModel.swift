//
//  LoginViewModel.swift
//  mivi_task
//
//  Created by Gnanavadivelu Pandurangan on 20/08/18.
//  Copyright © 2018 Gnanavadivelu Pandurangan. All rights reserved.
//

import Foundation

class LoginViewModel {
    
    static let shared = LoginViewModel()
    
    
    func getCollections(completion: @escaping APIManager.ServiceCallback) {
        
        APIManager.shared.getCollections { (response, error) in
            
            guard error == nil else {
                completion(nil, error);
                return;
            }
            
            if let response = response as? [String: Any] {
                
                let user = User();
                if let data = response["data"] as? [String: Any] {
                    if let attributes = data["attributes"] as? [String: Any] {
                        
                        user.id = data["id"] as? String;
                        user.fistName = (attributes["first-name"] as? String)!;
                        user.lastName =  attributes["last-name"] as? String;
                        user.dob =  attributes["date-of-birth"]  as? String;
                        user.email =  attributes["email-address"]  as? String;
                        user.email_verified = (attributes["email-address-verified"] as? Bool)!;
                        user.email_subscription = (attributes["email-subscription-status"] as? Bool)!;
                        
                        completion(user, nil);
                    }
                }
//                if let included = response["included"] as? [Any] {
//                    var properties = [Property]();
//
//                    for data in included {
//                        if let obj = data as? [String: Any] {
//                            let propery = Property();
//                            propery.type =  obj["type"] as? String;
//                            propery.id =  obj["id"] as? String;
//                            propery.attributes =  (obj["attributes"] as? [String: String])!
//
//                            properties.append(propery)
//                        }
//                    }
//                    user.included = properties;
//                }
                
            }
            completion(nil, error)
        }
        
    }
    
}
