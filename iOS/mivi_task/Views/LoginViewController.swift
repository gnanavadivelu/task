//
//  ViewController.swift
//  mivi_task
//
//  Created by Gnanavadivelu Pandurangan on 20/08/18.
//  Copyright © 2018 Gnanavadivelu Pandurangan. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var pwdTextField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    
    let viewModel = LoginViewModel.shared
    var activeUser: User?;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        viewModel.getCollections { (user, error) in
            if let user = user {
                self.activeUser = user as? User;
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func loginAction(_ sender: Any) {
        
        errorLabel.text = "";
        
        if !isValidForm() {
            errorLabel.text = "`Check your Email address and Password.`"
            return;
        }
        
        if activeUser?.email == emailTextField.text {
            self.performSegue(withIdentifier: "dashboard", sender: activeUser);
        } else {
            errorLabel.text = "`Check your Email address and Password.`"
            
        }
        
    }
    

    func isValidForm() -> Bool {
        let username =  self.emailTextField.text
        let password =  self.pwdTextField.text
        
        if !(username?.isEmail)! {
            return false
        }
      
        if (password?.isEmpty)! {
            return false
        }
        if ((password?.length)! < 6) {
            return false
        }
        return true
        
    }
}

extension LoginViewController: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            pwdTextField.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        errorLabel.text = "";
        
        if let whiteSpace = string.whiteSpace {
            return !whiteSpace
        }
        return true
    }
}

