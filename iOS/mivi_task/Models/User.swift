//
//  User.swift
//  mivi_task
//
//  Created by Gnanavadivelu Pandurangan on 20/08/18.
//  Copyright © 2018 Gnanavadivelu Pandurangan. All rights reserved.
//

import Foundation

class User: NSObject {
    var id: String?
    var fistName: String = "";
    var lastName: String?;
    var dob: String?
    var email: String?
    var email_verified: Bool = false
    var email_subscription: Bool = false;
    var included: [Property]!;
    
}
