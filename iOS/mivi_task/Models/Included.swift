//
//  Included.swift
//  mivi_task
//
//  Created by Gnanavadivelu Pandurangan on 20/08/18.
//  Copyright © 2018 Gnanavadivelu Pandurangan. All rights reserved.
//

import Foundation

class Property {
    var type: String!;
    var id: String!;
    var attributes:[String: String] = [:];
}
