//
//  APIManager.swift
//  mivi_task
//
//  Created by Gnanavadivelu Pandurangan on 20/08/18.
//  Copyright © 2018 Gnanavadivelu Pandurangan. All rights reserved.
//

import UIKit;


class APIManager {

    public typealias ServiceCallback = (Any?, MiviError?) -> Swift.Void

    static let shared = APIManager()
    
    static func session() -> URLSession {
        let config = URLSessionConfiguration.default;
        return URLSession(configuration: config)
    }
    
    func getCollections(completion: @escaping ServiceCallback){
        
        let collectionsEndPoint: String = "http://localhost/collection.json"
        guard let url = URL(string: collectionsEndPoint) else {
            return
        }
        
        let urlRequest = URLRequest(url: url)
        
        let session = APIManager.session();
        
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            guard error == nil else {
                completion(nil, .serverError);
                return
            }
            
        
            // Check response
            guard let responseData = data else {
                completion(nil, .invalidResponse);
                return
            }
          
            do {
                guard let collections = try JSONSerialization.jsonObject(with: responseData, options: [])
                    as? [String: Any] else {
                        completion(nil, .parsingError)
                        return
                }
                completion(collections, nil);

            } catch  {
                completion(nil, .parsingError)
                return
            }
        }
        task.resume()
        
    }
    
}

enum MiviError: Error {
    case unknownError
    case connectionError
    case invalidCredentials
    case invalidRequest
    case notFound
    case invalidResponse
    case parsingError
    case serverError
    case serverUnavailable
}

public enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case patch = "PATCH"
    case delete = "DELETE"
}
